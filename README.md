![npm](https://img.shields.io/npm/v/autoprefixer)
![Upgrade](https://img.shields.io/badge/upgrade-CSS-blueviolet)
![Learn](https://img.shields.io/badge/learn-SASS-ff69b4)
![Learn](https://img.shields.io/badge/learn-NodeJS-green)

# ![alt text](public/images/logo/ohmyfood.png)
## Directory : renaldBezet_3_30112020
### **Recommandations**
#### *Utiliser NodeJS, NPM et Sass*:
Vérifier si Node et/ou sass sont déjà installés, aller dans le terminal/shell: <br/>
 `node -v` <br/>`sass -v`<br/>
 Si nécessaire suivre les différentes instructions suivantes <br/>
##### .NodeJS :
Aller sur la page https://nodejs.org/fr/download/package-manager/, puis sélectionner selon son environnement. Suivre les instructions données sur le site. NPM sera installé avec.
##### .NPM: 
1. Vous pouvez vérifier la version de NPM:<br/>`npm -v`<br/>
2. Pour mettre à jour<br/>`npm install npm@latest -g` <br/>
3. Initialiser un fichier JSON ( JavaScrit Object Node) dans le projet pour utiliser Sass, Autoprefixer, etc. Dans le terminal/shell:<br/>
`npm init`<br/>
Puis remplir les différents champs, ils pourront être modifiés par la suite. taper Entrée à la fin.<br/>
.Sass<br/>
1. Dans le terminal/shell: <br/>
`npm install sass -g`<br/>
2. Dans le fichier JSON, à la ligne "script", préciser l'endroit où trouver le Sass à compiler et le fichier de destination: <br/>`"sass": "sass ./sass/main.scss:./public/css/style.css --watch --style compressed"`
3. Exécuter Sass à chaque utilisation en tapant dans le terminal/shell:<br/>`npm run sass` <br/>
4. Pour Autoprefixer la démarche sera la même que Sass. C'est un plugin qui ajoute automatiquement des préfixes dans votre CSS, Dans le terminal  <br/>
`npm install autoprefixer postcss postcss-cli -g`<br/>
puis dans le fichier JSON <br/>Ajouter à `scripts{}`:<br/>
`"prefix": "postcss ./public/css/style.css --use autoprefixer -d ./public/css/prefixed/"`<br/>
puis au fichier JSON ajouter la ligne:<br/>`"browserslist": "last 4 versions"`<br/>
Exécuter dans le terminal avec<br/> `npm run prefix`

 






# 1. Première lecture du projet.
## 1.1 Présentation de l'entreprise
**Ohmyfood!** est une entreprise de commande de repas en ligne. Le concept permet aux utilisateurs de composer leur propre menu et réduire leur temps d’attente dans les restaurants car leur menu est préparé à l’avance.  
Être identifiée comme une entreprise proposant des *services haut de gamme*.  
La cible sont les personnes connectées et pressées.
## 1.2 Enjeux 🎯
* Phase 1 : Développer un site proposant le menu de 4 grands restaurants parisiens.
* Phase 2 : Permettre la réservation en ligne et la composition de menus.
## 1.3 Le cahier des charges 📔
### 1.3.1 Axes principaux
* Date de livraison de la première version du site : sous 1 mois. Le développement devra se faire en **CSS**, sans *JavaScript* .
* Approche **mobile-first** 
* Aucun framework ne devra être utilisé, en revanche l’utilisation de **Sass** serait un plus.
* Aucun code **CSS** ne devra être appliqué via un attribut style dans une balise **HTML**.
* Les pages devront passer la validation W3C en **HTML** et **CSS** sans erreur.
* Le site doit être parfaitement compatible avec les dernières versions desktop de *Chrome* et *Firefox*.
### 1.3.2 Contenu du site et fonctionalités 📝
#### 1.3.2.1 **Page d’accueil** (x1)
* Affichage de la localisation des restaurants. À terme il sera possible de choisir sa localisation pour trouver des restaurants proches d’un certain lieu.
* Une courte présentation de l’entreprise.
* Une section contenant les 4 menus sous forme cartes. Au clic sur la carte,
* L’utilisateur est redirigé vers la page du menu.
#### **1.3.2.2 Pages de menu** (x4)
____
4 pages contenant chacune le menu d’un restaurant.
1. Header  
Le header est présent sur toutes les pages.
Sur la page d’accueil, il contient le logo du site.
2. Menu  
Sur les pages, il contient en plus un bouton de retour vers la page d’accueil
3. Footer  
Le footer est identique sur toutes les pages.
Au clic sur “Contact”, un renvoi vers une adresse mail est effectué.
                
# 2. Observation des maquettes et balisage (HTML5)
## 2.1 Repérage des parties
Accueil.pdf  
Menu - À la française.pdf
## 2.2 Structure Accueil
    <body>
        <div id="loading"></div>
        <header>
        </header>
        <main>
            <section id= "find-restaurant">
            </section>
            <section id="container">
                <aside id="user-wiki"></aside>
                <article id="restaurants"></article>
            </section>
        </main>
        <footer> 
        </footer>  
## 2.3 Structure Menu
    <header>
    </header>
    <main>
        <section class="banner">
        </section>
        <section class="menu">
        </section>
    </main>        
    <footer> 
        <h3>
        <ul>
    </footer>
# 3. Observation du style & organisation SASS
## 3.1 Identité graphique
### 3.1.1 Polices
Logo et titres: Shrikhand  
Texte: Roboto
### 3.1.2 Couleurs  🌈 
Primaire: `#9356DC` ==> purple  
Secondaire: `#FF79DA` ==>  pink  
Tertiaire: `#99E2D0` ==> green 
## 3.2 Les blocs
### 3.2.1 header
Quand l’application aura plus de menus, un “loading spinner” sera nécessaire.  
Sur cette maquette, nous souhaitons en avoir un aperçu. Il devra apparaître pendant 1 à 3 secondes quand on arrive sur la page d'accueil, couvrir l'intégralité de l'écran, et utiliser les animations CSS (pas de librairie).   
Le design de ce loader n’est pas défini, toute proposition est donc la bienvenue tant qu’elle est cohérente avec la charte graphique du site.
### 3.2.2 banner
### 3.2.3 restaurant
### 3.2.4 menu
À l’arrivée sur la page, les plats devront apparaître progressivement avec un léger décalage dans le temps. Ils pourront soit apparaître un par un, soit par groupe “Entrée”, “Plat” et “Dessert”. Un exemple de l’effet attendu est fourni.  
Le visiteur peut ajouter les plats qu'il souhaite à sa commande en cliquant dessus. Cela fait apparaître une petite coche à droite du plat. Cette coche devra coulisser de la droite vers la gauche. Pour cette première version, l’effet peut apparaître au survol sur desktop au lieu du clic. Si l’intitulé du plat est trop long, il devra être rogné avec des points de suspension. Un exemple de l’effet attendu est fourni.
### 3.2.5 footer
## 3.3 Page spécifique 
Accueil : restaurant / card
## 3.4 Composant
### 3.4.1 bouton
Au survol, la couleur de fond des boutons principaux devra légèrement s’éclaircir. L’ombre portée devra également être plus visible.  
Pour les restaurants:  
Le bouton "J’aime" en forme de cœur est présent sur la maquette. Au clic, il devra se remplir progressivement. Pour cette première version, l’effet peut être apparaître au survol sur desktop au lieu du clic.
### 3.4.2 card
doit comporter la photo illustrative du restaurant et avoir un lien vers la page menu du restaurant
## 3.5 Les variables
Elles sont dans le dossier base. Les autres variables à portée locale seront écrites par fichier. cela vaut pour :
- Colorisation
- Typographie et graphie

## 3.6 les media queries 
un fichier mixin.scss dans le dossier utils regroupe les principales media queries. <br/>
le code suivant a été utilisé pour implémenter et modifier selon les corrections et l'évolution du projet.: </br> `@mixin screen and (taille écran) { @content}` </br>

# 4. Arborescence du Projet
    Projet          
        |-public 
            |-index.html
            |--html
                |---nom_restaurant1.html
                |---nom_restaurant2.html
                |---nom_restaurant3.html         
                |---nom_restaurant4.html          
            |---css
                |--prefixed
                    |---style.css     
                |---style.css     
                |---style.css.map      
            |--images
        |-sass
            |---main.scss
            |--base
                |-- _typography.scss                      
                |-- _colors.scss
            |--components
                |-- _btn.scss
                |-- _card.scss 
                |-- _icon.scss
            |--layout
                |-- _header.scss
                |-- _banner.scss
                |-- _footer.scss
                |-- _menu.scss
            |--pages
                |-- _index.scss    
            |--utils
                |-- _btneffect.scss
                |-- _loadspinner.scss
                |-- _delay_appear.scss
                |-- _iconeffect.scss
                |-- _mixin.scss
            |--vendors
                |-- _libraries.scss
        |---package.json
        |---package-lock.json
        |---gitlab-ci.yml
        |---.gitignore
        |---README.md
